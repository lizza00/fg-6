package com;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Jdbc {

    private Connection connection;
    private Statement statement;

    public void connection(String db) throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:" + db);
        statement = connection.createStatement();
    }

    public boolean validateTableName(String tableName) throws SQLException {
        ResultSet rs = connection.getMetaData().getTables(null, null, tableName, null);
        while (rs.next()) {
            if (rs.getString("TABLE_NAME").equals(tableName)) {
                return true;
            }
        }
        return false;
    }

    public boolean validateAttribute(String tableName, String attribute) throws SQLException {
        ResultSet rs = connection.getMetaData().getColumns(null, null, tableName, attribute);
        while (rs.next()) {
            if (rs.getString("COLUMN_NAME").equals(attribute)) {
                return true;
            }
        }
        return false;
    }

    public boolean validateAttributesMinus(final String tableName1, List<String> attributes1, String tableName2, List<String> attributes2) throws SQLException {
        if (validateTableName(tableName1) && validateTableName(tableName2) && attributes1.size() == attributes2.size()) {
            try {
                attributes1.stream().forEach(a -> {
                    try {
                        if (!validateAttribute(tableName1, a)) {
                            throw new SQLException(a + " not found");
                        }
                    } catch (SQLException e) {
                        throw new IllegalArgumentException(e.getMessage());
                    }
                });

                attributes2.stream().forEach(a -> {
                    try {
                        if (!validateAttribute(tableName2, a)) {
                            throw new SQLException(a + " not found");
                        }
                    } catch (SQLException e) {
                        throw new IllegalArgumentException(e.getMessage());
                    }
                });

            } catch (IllegalArgumentException ex) {
                return false;
            }

        } else {
            return false;
        }
        return true;
    }

    public void select(String sql) throws SQLException {
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                System.out.print(resultSet.getObject(i) + "    ");
            }
            System.out.println();
        }
    }

    public void create(String sql) throws SQLException {
        statement.execute(sql);
    }

    public void close() throws SQLException {
        statement.close();
        connection.close();
    }
}