package com;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Main {
    private static Jdbc jdbc;

    private static   String input;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {


        jdbc = new Jdbc();
        jdbc.connection("db");


        Scanner in = new Scanner(System.in);
        input = in.nextLine();
        String[] words = input.split("[ (),]+");
        if (!validate(words)) {

        }
        jdbc.close();


    }

    private static boolean validate(String[] words) throws SQLException {
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals("minus")) {
                List<String> l1 = new ArrayList<>();
                for (int j = 1; j < i; j++) {
                    if (words[j].startsWith("[")) {
                        words[j] = words[j].substring(1);
                    }
                    if (words[j].endsWith("]")) {
                        words[j] = words[j].substring(0, words[j].length() - 1);
                        l1.add(words[j]);
                        break;
                    }
                    l1.add(words[j]);
                }

                List<String> l2 = new ArrayList<>();
                for (int j = i + 2; j < words.length; j++) {
                    if (words[j].startsWith("[")) {
                        words[j] = words[j].substring(1);
                    }
                    if (words[j].endsWith("]")) {
                        words[j] = words[j].substring(0, words[j].length() - 1);
                        l2.add(words[j]);
                        break;
                    }
                    l2.add(words[j]);
                }
                if (!jdbc.validateAttributesMinus(words[0], l1, words[i + 1], l2)) {
                    System.out.println("Not valid");
                    return false;
                }
                System.out.println("OK");
                return true;
            }
        }
        if (words[0].equals("drop")) {
            if (!jdbc.validateTableName(words[2])) {
                System.out.println(words[2] + " table not found");
            } else {
                System.out.println(input);
                jdbc.create(input);
                System.out.print("OK");
                return true;
            }
        } else if (words[0].equals("new")) {
            if (jdbc.validateTableName(words[2])) {
                System.out.println(words[2] + " table already exists");
            } else {
                Set<String> uniqueAttributes = new HashSet<>();
                for (int i = 3; i < words.length; i += 2) {
                    uniqueAttributes.add(words[i]);
                }
                if ((words.length - 3) / 2 > uniqueAttributes.size()) {
                    System.out.println("Attributes not unique");
                    return false;
                }
                String sql = input.replace("new", "create");
                System.out.println(sql);
                jdbc.create(sql);
                System.out.print("OK");

                return true;
            }
        } else if (words[1].equals("join")) {
            if (!jdbc.validateTableName(words[0])) {
                System.out.println(words[0] + " table not found");
            } else if (!jdbc.validateTableName(words[2])) {
                System.out.println(words[2] + " table not found");
            } else {
                if (!jdbc.validateAttribute(words[0], words[4])) {
                    System.out.println(words[4] + " attribute not found");
                } else if (!jdbc.validateAttribute(words[2], words[6])) {
                    System.out.println(words[6] + " attribute not found");
                } else {
                    System.out.print("OK");
                    return true;
                }
            }
        } else if (words[1].startsWith("[")) {
            String sql = "select %s from " + words[0];
            String str = "";
            if (!jdbc.validateTableName(words[0])) {
                System.out.println(words[0] + " table not found");
            } else {
                for (int i = 1; i < words.length; i++) {

                    if (words[i].startsWith("[")) {
                        words[i] = words[i].substring(1);

                    }
                    if (words[i].endsWith("]")) {
                        words[i] = words[i].substring(0, words[i].length() - 1);
                    }
                    if (str.length() != 0 && !str.endsWith(", ")) {
                        str += ", ";
                    }
                    str += words[i];
                    if (!jdbc.validateAttribute(words[0], words[i])) {
                        System.out.println(words[i] + " attribute not found");
                        return false;
                    }
                }
                System.out.println("OK");
                System.out.println(String.format(sql, str));
                jdbc.select(String.format(sql, str));
                return true;
            }
        }
        return true;
    }
}
